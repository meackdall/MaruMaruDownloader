package comicobj;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom2.Element;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import fileaccess.XMLHandler;

/**
 * 에피소드 객체 정의
 */
public class Episode {
	private String name;				// 에피소드 이름
	private String url;					// 에피소드의 URL
	
	public static final int StatusNormal = 0;
	public static final int StatusExist = 1;
	public static final int StatusLock = 2;
	public static final int StatusError = 3;
	
	public static final String user_agent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";
	
	// 생성자 정의
	public Episode(String name, String url) {
		this.name = name;
		this.url = url;
	}
	
	// 에피소드의 이름을 반환하는 메소드
	public String GetName() {
		return this.name;
	}
	
	// 에피소드의 URL을 반환하는 메소드
	public String GetUrl() {
		return this.url;
	}
	
	/* 
	 * 에피소드 하나를 저장하는 메소드
	 * 1. download_list.xml에 해당 에피소드가 존재하는지 확인
	 * 2. 존재하지 않는다면 해당 에피소드를 다운로드
	 * 3. download_list.xml에 에피소드를 추가하고 메소드 종료
	 */
	public int SaveEpisode(String comics_name) {
		XMLHandler xmlhandler = new XMLHandler();

		Element comics_element = xmlhandler.getComicsElement(comics_name);
		List<Element> episode_list = comics_element.getChildren("episode");
		
		boolean enablecookie = xmlhandler.getCookieEnabled();
		String stringcookie = xmlhandler.getCookie();
		int retry_ctn = 0;
		while(true) {
			try {
				// XML의 만화 항목에서 에피소드 이름을 검색
				for (Element episode_e : episode_list) {
					// 만약 존재한다면 메소드 종료
					if (episode_e.getChild("episode_name").getText().equals(name)) {
						return Episode.StatusExist;
					}
				}

				/* 아래부터는 에피소드 이름이 존재하지 않을 경우 */

				// 에피소드 디렉토리 확인
				// 존재하지 않는 디렉토리이면 새로 생성
				// 존재하는 디렉토리일 경우 해당 디렉토리의 모든 파일 삭제
				String episode_dir = xmlhandler.getSavePath() + XMLHandler.ReplaceInvalidChar(comics_name) 
				+ File.separator + XMLHandler.ReplaceInvalidChar(name) + File.separator;
				File episode_dir_file = new File(episode_dir);
				if (!episode_dir_file.exists()) {
					episode_dir_file.mkdirs();
				}
				else {
					File[] delete_files = episode_dir_file.listFiles();
					for(File d_file : delete_files) {
						d_file.delete();
					}
				}

				/* 이미지 다운로드 및 저장 */

				// 에피소드 URL을 통해 HTML 페이지를 가져옴
				// 패스워드가 걸린 만화를 구분하기 위해 리다이렉트를 허용
				Response response = Jsoup.connect(url).followRedirects(true).timeout(10000).execute();
				org.jsoup.nodes.Document block_page = response.parse();
				String response_url = response.url().toExternalForm();
				Pattern password_pattern = Pattern.compile(".*type=pass");
				Matcher matcher = password_pattern.matcher(response_url);
				
				org.jsoup.nodes.Document episode_page;
				Elements episode_imgs;
				
				// 패스워드가 걸린 만화일 경우
				boolean isPassword = matcher.find();
				if (isPassword) {
					// 쿠키 값을 사용하지 않을 경우 바로 반환
					if (!enablecookie) {
						return Episode.StatusLock;
					}
					
					// 쿠키값을 변조해 패스워드 통과
					// 브라우저로 패스워드를 통과한 쿠키값을 받아와야만 함
					Map<String, String> cookie = response.cookies();
					cookie.put("PHPSESSID", stringcookie);
					
					episode_page = Jsoup.connect(url)
							.header("content-type", "application/x-www-form-urlencoded")
							.header("origin", "http://wasabisyrup.com")
							.userAgent(Episode.user_agent)
							.referrer(response_url)
							.cookies(response.cookies())
							.timeout(5000)
							.userAgent("Mozilla/5.0").post();
					 
					/* 패스워드를 통과 */
					
					// div 클래스가 gallery-template인 항목에서
					// data-signature와 data-key를 가져옴
					org.jsoup.nodes.Element gallery_template = episode_page.select("div.gallery-template").first();
					String data_signature = gallery_template.attr("data-signature");
					String data_key = gallery_template.attr("data-key");
					
					// 이미지 목록을 요청할 URL 구성
					// 파라미터들은 URL 인코딩
					String locked_list_url = "http://wasabisyrup.com/assets/"
							+ url.substring(url.indexOf("archives/") + 9, url.length())
							+ "/1.json?signature=" + URLEncoder.encode(data_signature, "UTF-8")
							+ "&key=" + URLEncoder.encode(data_key, "UTF-8");
					
					URLConnection lock_list_ucon = new URL(locked_list_url).openConnection();
					lock_list_ucon.setRequestProperty("referer", url);
					lock_list_ucon.setRequestProperty("user-agent", Episode.user_agent);
					lock_list_ucon.setRequestProperty("Host", "wasabisyrup.com");
					lock_list_ucon.setRequestProperty("cookie", cookie.toString());
					
					// 요청한 인자는 JSON 형식이므로 JSON 파싱
					InputStreamReader lock_list_in = new InputStreamReader(lock_list_ucon.getInputStream(), "UTF-8");
					JSONObject resp_json = (JSONObject)JSONValue.parse(lock_list_in);
					JSONArray resp_arr = (JSONArray) resp_json.get("sources");
					
					episode_imgs = new Elements();
					
					for(int i = 0; i < resp_arr.size(); i++) {
						org.jsoup.nodes.Element episode_img = new org.jsoup.nodes.Element("ol");
						episode_img.attr("data-src", resp_arr.get(i).toString());
						episode_imgs.add(episode_img);
					}
				} else {
					// 비밀번호가 걸리지 않았다면
					// HTML parser를 통해 lz-lazyload 클래스를 가진 모든 이미지(img)를 추출
					episode_page = block_page;
					episode_imgs = episode_page.select("img[class=lz-lazyload]");
				}

				int serial = 0;
				// 이미지들을 순회하며 파일로 다운로드 받아 저장
				for (org.jsoup.nodes.Element episode_img : episode_imgs) {
					// 파일명 = /다운로드경로/만화이름/에피소드이름/에피소드이름_XXX.jpg
					// 패스워드가 있는 경우 Host URL + JSON으로 받은 URL
					// 패스워드가 없는 경우 data-src의 절대 경로 추출
					// URL에서 파일을 내려받을 InputStream 선언
					// 파일로 저장할 OutputStream 선언
					String episode_img_name = episode_dir + XMLHandler.ReplaceInvalidChar(name) + String.format("_%03d.jpg", serial);
					URL url;
					if (isPassword) {
						url = new URL(("http://wasabisyrup.com" + episode_img.attr("data-src")).replaceAll(" ", "%20"));
					} 
					else {
						url = new URL(episode_img.attr("abs:data-src").replaceAll(" ", "%20"));
					}
					
					HttpURLConnection uCon = null;
					InputStream uin = null;
					OutputStream fout = new BufferedOutputStream(new FileOutputStream(episode_img_name));;
					
					uCon = (HttpURLConnection) url.openConnection();
					// 브라우저 속성을 추가하지 않으면 403 Error가 발생
					// 호스트 및 참조자 추가
					uCon.setRequestProperty("User-Agent", Episode.user_agent);
					uCon.setRequestProperty("Host", "wasabisyrup.com");
					uCon.setRequestProperty("Referer", this.GetUrl());
					uCon.setConnectTimeout(5000);
					uCon.setReadTimeout(5000);
					uin = uCon.getInputStream();
					
					// byteRead = 읽은 byte 크기
					// buff = byte 버퍼
					int byteRead = 0;
					int buff_size = 4096;
					byte[] buff = new byte[buff_size];

					// 이미지 다운로드
					while((byteRead = uin.read(buff)) != -1) {
						fout.write(buff, 0, byteRead);
					}
					
					// 다운로드 받은 파일이 0바이트일 경우 1초 후 다시 시도
					File checkfile = new File(episode_img_name);
					if (checkfile.length() == 0) {
						Thread.sleep(1000);
						continue;
					}
					
					uin.close();
					fout.close();
					serial++;

					// 이미지 하나를 다운로드받고 1초간 대기
					Thread.sleep(1000);
				}

				// download_list.xml에 해당 에피소드를 저장
				Element episode_element = new Element("episode");
				Element episode_name_element = new Element("episode_name");
				Element episode_url_element = new Element("episode_url");
				episode_name_element.setText(name);
				episode_url_element.setText(url);
				episode_element.addContent(episode_name_element);
				episode_element.addContent(episode_url_element);
				comics_element.addContent(episode_element);

				// XML에 저장 후 종료
				xmlhandler.SaveXMLFile();
				xmlhandler.close();
				return Episode.StatusNormal;
			} catch(SocketTimeoutException sockettimeoute) {
				// 예외 발생시 10초 후 재시도
				// 3번 재시도하고 실패하면 종료
				if (retry_ctn < 3) {
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					retry_ctn++;
				}
				else {
					xmlhandler.close();
					return Episode.StatusError;
				}
			} catch(Exception e) {
				e.printStackTrace();
				return Episode.StatusError;
			}
		}
	}
}
