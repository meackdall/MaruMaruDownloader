package comicobj;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.util.regex.*;

/** 
 * 만화 객체 정의
 * 만화 > 에피소드의 포함관계가 성립
 * 만화 - 에피소드 1, 2, ... , N 
 */
public class Comics {
	private String name;						// 만화이름
	private String url;							// 만화의 메인 URL
	private ArrayList<Episode> episode_list; 	// 에피소드 리스트
	
	// 생성자 정의
	public Comics(String name, String url) {
		this.name = name;
		this.url = url;
		this.episode_list = new ArrayList<Episode>();
	}
	
	// 만화 이름을 반환하는 메소드
	public String GetName() {
		return this.name;
	}
	
	// 만화의 메인 URL을 반환하는 메소드
	public String GetUrl() {
		return this.url;
	}
	
	// 만화의 메인 URL에 접근해 에피소드 리스트를 생성하는 메소드
	public int CreateEpisodeList() {
		// 메인 URL을 통해 HTML 페이지를 가져옴
		int retry_ctn = 0;
		while (true) {
			try {
				org.jsoup.nodes.Document comics_page = Jsoup.connect(url).timeout(10000).get();
				// HTML parser를 통해 에피소드의 url 목록을 추출
				// id = vContent 인 div 추출
				// 해당 div에서 하이퍼링크 추출
				Elements vContent_div = comics_page.select("div#vContent");
				Elements episode_url_list = vContent_div.select("a");

				// 에피소드 URL 패턴 "http:// ... /archieves/ ..." 정의
				Pattern episode_pattern = Pattern.compile("http://.*/archives/.*");

				for (int i = 0; i < episode_url_list.size(); i++) {
					String episode_url = episode_url_list.get(i).attr("abs:href");
					String episode_name = episode_url_list.get(i).text();
					Matcher episode_matcher = episode_pattern.matcher(episode_url);
					// 에피소드 URL 패턴이 일치할 경우에만 에피소드로 간주
					if (episode_matcher.find()) {
						Episode episode = new Episode(episode_name, episode_url);
						AddEpisode(episode);
					}
				}
				return Episode.StatusNormal;
			} catch (SocketTimeoutException socketexception) {
				// Timeout 발생시 10초 후 재시도
				// 3번 재시도하고 실패하면 종료
				if (retry_ctn < 3) {
					for (int i = 10; i > 0; i--) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				else {
					return Episode.StatusError;
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				return Episode.StatusError;
			}
		}
	}
	
	/**
	 * 만화에 에피소드를 하나를 추가하는 메소드
	 */
	public void AddEpisode(Episode episode) {
		// 이미 존재하는 이름의 에피소드는 추가하지 않음
		if (!ContainsEpisode(episode.GetName())) {
			this.episode_list.add(episode);
		}
	}
	
	/**
	 * 만화에 해당 에피소드가 존재하는지 확인하는 메소드
	 * 오브젝트의 리스트이기 때문에 이름을 비교
	 */
	public boolean ContainsEpisode(String name) {
		int i = 0;
		for (i = 0; i < this.episode_list.size(); i++) {
			if (this.episode_list.get(i).GetName().equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 에피소드 리스트를 반환하는 메소드
	 */
	public ArrayList<Episode> getEpisodeList() {
		return episode_list;
	}
}
