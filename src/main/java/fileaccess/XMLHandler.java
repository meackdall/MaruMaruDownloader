package fileaccess;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import comicobj.Comics;

/**
 *  XML 파일을 조작할 핸들러 정의
 */
public class XMLHandler {
	private File download_list_file;
	private FileInputStream download_list_fin;
	private SAXBuilder download_list_builder;
	private Document download_list_doc;
	
	// 루트 노드, 썸네일 노드, 썸네일의 리스트 노드
	private Element download_list_root;
	private Element thumbnails_element;
	private List<Element> thumbnails_list;
	
	private Attribute save_path;
	private Attribute thumbnails_path;
	
	public List<Element> comics_list;
	
	private Element cookie_element;
	private boolean enablestartup;
	private Element lock_element;
	
	/**
	 * 바로가기 파일을 현재 디렉토리에 복사해두는 메소드
	 */
	public void copyLnk() {
		File startupfile = new File(PathInfo.getLnkPath());
		File cpfile = new File(PathInfo.getCurrentDirectory() + "다우마루.lnk");
		if (!cpfile.isFile()) {
			try {
				FileInputStream fin = new FileInputStream(startupfile);
				FileOutputStream fout = new FileOutputStream(cpfile);
				byte[] readByte = new byte[4096];
				while(fin.read(readByte) != -1) {
					fout.write(readByte);
				}

				fin.close();
				fout.close();
				if (!getStartup()) {
					startupfile.delete();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 파일명으로 사용할 수 없는 문자를 모두 '@'로 변환하는 메소드
	 * 객체를 생성하지 않고도 사용할 수 있도록 정적 메소드로 정의
	 * 사용할 수 없는 문자 \, /, :, ?, ", <, >, |
	 */
	public static String ReplaceInvalidChar(String oldstr) {
		Pattern pattern = Pattern.compile("[\\/:[*]?\"<>[|]]");
		Matcher matcher = pattern.matcher(oldstr);
		String newstr = matcher.replaceAll("@");
		return newstr;
	}
	
	/**
	 * 대괄호와 그 안의 내용을 모두 제거하는 메소드
	 */
	public static String RemoveBrackets(String oldstr) {
		Pattern pattern = Pattern.compile("\\[.*\\]");
		Matcher matcher = pattern.matcher(oldstr);
		return matcher.replaceAll("");
	}
	
	/**
	 * 썸네일을 작은 크기로 변경해 반환하는 메소드
	 * 예외 발생시 썸네일 실패 이미지를 보여줌
	 */
	public static ImageIcon GetResizedImage (File thumbnail_file, int width, int height) {
		ImageIcon resized_icon = null;
		BufferedImage bfimg = null;
		try {
			bfimg = ImageIO.read(thumbnail_file);
			Image dimg = bfimg.getScaledInstance(width, height, Image.SCALE_SMOOTH);
			resized_icon = new ImageIcon(dimg);
		}
		catch (Exception e) {
			try {
				bfimg = ImageIO.read(new File(PathInfo.getThumbnailFailedPath()));
				Image dimg = bfimg.getScaledInstance(width, height, Image.SCALE_SMOOTH);
				resized_icon = new ImageIcon(dimg);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return resized_icon;
	}
	
	// 생성자 정의
	// 생성과 동시에 파일을 읽어
	// 루트 노드, 썸네일 목록, 만화 저장 경로, 썸네일 경로를 세팅
	public XMLHandler() {
		try {
			download_list_file = new File(PathInfo.getDownloadListPath());
			download_list_fin = new FileInputStream(download_list_file);
			download_list_builder = new SAXBuilder();
			download_list_doc = download_list_builder.build(download_list_fin);
			
			download_list_root = download_list_doc.getRootElement();
			thumbnails_element = download_list_root.getChild("thumbnails");
			thumbnails_list = thumbnails_element.getChildren();
			
			save_path = download_list_root.getAttribute("save_path");
			thumbnails_path = thumbnails_element.getAttribute("thumbnails_path");
			
			comics_list = download_list_root.getChildren("comics");
			
			cookie_element = download_list_root.getChild("cookie");
			
			enablestartup = Boolean.valueOf(download_list_root.getAttribute("startup").getValue());
			
			lock_element = download_list_root.getChild("lock");
			
			checkPath();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void checkPath() {
		if (save_path.getValue().equals("")) {
			download_list_root.setAttribute("save_path", PathInfo.getDefaultSaveDirectory());
		}
		
		if (thumbnails_path.getValue().equals("")) {
			thumbnails_element.setAttribute("thumbnails_path", PathInfo.getThumbnailsDirectory());
		}
	}
	
	/**
	 * XML을 저장하는 메소드
	 */
	public void SaveXMLFile() {
		try {
			XMLOutputter xout = new XMLOutputter();
			Format xformat = xout.getFormat();
			xformat.setEncoding("UTF-8");
			xformat.setIndent(" ");
			xformat.setLineSeparator("\r\n");
			xformat.setTextMode(Format.TextMode.TRIM);

			xout.setFormat(xformat);
			xout.output(download_list_doc, new FileWriter(PathInfo.getDownloadListPath()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 입력 스트림 종료
	 */
	public void close() {
		try {
			download_list_fin.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * XML의 루트 노드를 반환하는 메소드
	 */
	public Element getRoot() {
		return download_list_root;
	}
	
	
	/**
	 * XML에서 만화를 검색해 노드를 반환
	 */
	public Element getComicsElement(String comics_name) {
		Element comics_element = null;

		// xml에서 만화 이름을 검색
		for (Element comics_e : comics_list) {
			if (comics_e.getChild("comics_name").getText().equals(comics_name)) {
				comics_element = comics_e;
			}
		}
		return comics_element;
	}
	
	/**
	 * XML에서 선택된 만화 목록을 업로드하는 메소드
	 */
	public HashMap<String, Comics> LoadSelectedComicsList() {
		HashMap<String, Comics> selected_comics_map = new HashMap<String, Comics>();
		try {
			// comics 노드를 모두 조회해 리스트에 추가
			for (Element comics_e : comics_list) {
				String comics_name = comics_e.getChildText("comics_name").toString();
				String comics_url = comics_e.getChildText("comics_url").toString();
				selected_comics_map.put(comics_name, new Comics(comics_name, comics_url));
			}
			return selected_comics_map;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 선택된 만화 목록을 XML에 세팅하는 메소드
	 */
	public void SaveSelectedComics(HashMap<String, Comics> selected_comics_map) {
		try {
			// XML의 모든 만화를 순회하며 선택된 만화 목록에 없을 경우 삭제
			for (Element comics_e : comics_list) {
				String comics_name = comics_e.getChildText("comics_name");
				if (!selected_comics_map.containsKey(comics_name)) {
					download_list_root.removeContent(comics_e);
				}
			}
			
			// 선택된 만화 목록을 순회하며 XML에 존재하지 않는 만화일 경우 추가
			Iterator<String> selected_comics_map_iter = selected_comics_map.keySet().iterator();
			while (selected_comics_map_iter.hasNext()) {
				String selected_comics_name = selected_comics_map_iter.next();
				Comics selected_comics = selected_comics_map.get(selected_comics_name);
				boolean isContainsList = false;
				// XML에 해당 만화가 있는지 확인
				for (Element comics_e : comics_list) {
					if (selected_comics_name.equals(comics_e.getChildText("comics_name").toString())) {
						isContainsList = true;
						break;
					}
				}
				// 없다면 추가
				if (!isContainsList) {
					Element ncomics_e = new Element("comics");
					Element ncomics_name_e = new Element("comics_name");
					Element ncomics_url_e = new Element("comics_url");

					ncomics_name_e.setText(selected_comics.GetName());
					ncomics_url_e.setText(selected_comics.GetUrl());

					ncomics_e.addContent(ncomics_name_e);
					ncomics_e.addContent(ncomics_url_e);

					download_list_root.addContent(ncomics_e);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 썸네일 경로 리스트를 반환하는 메소드
	 */
	public List<Element> LoadThumbnailPathList() {
		return thumbnails_list;
	}
	
	/**
	 * XML에 썸네일 경로를 세팅하는 메소드
	 */
	public void SaveThumbnailPath(String thumbnail_path) {
		// 다운로드가 완료되면 XML에 경로 저장
		org.jdom2.Element thumbnail_element = new org.jdom2.Element("thumbnail_name");
		thumbnail_element.setText(thumbnail_path);
		thumbnails_element.addContent(thumbnail_element);
	}
	
	/**
	 * 만화의 저장 위치를 반환하는 메소드
	 */
	public String getSavePath() {
		return save_path.getValue();
	}
	
	/**
	 * 썸네일 저장 위치를 반환하는 메소드
	 */
	public String getThumbnailPath() {
		return thumbnails_path.getValue();
	}
	
	/**
	 * 쿠키 사용 여부를 반환하는 메소드
	 */
	public boolean getCookieEnabled() {
		return Boolean.valueOf(cookie_element.getAttribute("enable").getValue());
	}
	
	/**
	 * 만화의 저장 위치를 세팅하는 메소드
	 */
	public void setSavePath(String save_path) {
		download_list_root.setAttribute("save_path", save_path);
	}
	
	/**
	 * 쿠키값을 반환하는 메소드
	 */
	public String getCookie() {
		return cookie_element.getChildText("phpsession");
	}
	
	/**
	 * 쿠키값을 세팅하는 메소드
	 */
	public void setCookie(String phpsession) {
		cookie_element.getChild("phpsession").setText(phpsession);
	}
	
	/**
	 * 쿠키값 사용 여부를 세팅하는 메소드
	 */
	public void EnableCookie(boolean flag) {
		cookie_element.getAttribute("enable").setValue(String.valueOf(flag));
	}
	
	/**
	 * 자동 업데이트를 사용하는지 여부를 반환하는 메소드
	 */
	public boolean getStartup() {
		return enablestartup;
	}
	
	/**
	 * 자동 업데이트 사용 여부를 세팅하는 메소드
	 */
	public void setStartup(boolean enablestartup) {
		download_list_root.setAttribute("startup", String.valueOf(enablestartup));
	}
	
	/**
	 * 프로그램 잠금 사용 여부를 반환하는 메소드
	 * 평문 "true", "false" 대신 해당 문자열을 암호화해 저장
	 * 만약 손상되었을 경우 -1 반환 
	 */
	public int getEnableLock() {
		String encrypted_true = encryptPassword(String.valueOf(true).toCharArray());
		String encrypted_false = encryptPassword(String.valueOf(false).toCharArray());
		if (lock_element.getAttribute("enable").getValue().equals(encrypted_true)) {
			return 1;
		}
		else if (lock_element.getAttribute("enable").getValue().equals(encrypted_false)) {
			return 0;
		}
		else {
			return -1;
		}
	}
	
	/**
	 * 프로그램 잠금 사용 여부를 세팅하는 메소드
	 */
	public void setEnableLock(boolean enablelock) {
		String enablelock_encrypted = encryptPassword(String.valueOf(enablelock).toCharArray());
		lock_element.setAttribute("enable", enablelock_encrypted);
	}
	
	public String encryptPassword(char[] plaintext) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			byte[] bytes = new String(plaintext).getBytes(Charset.forName("UTF-8"));
			md.update(bytes);
			md.digest();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 프로그램 비밀번호가 맞는지 확인하는 메소드
	 */
	public boolean isCorrectPassword(char[] password) {
		if (encryptPassword(password).equals(lock_element.getChildText("password"))) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 프로그램 비밀번호를 세팅하는 메소드
	 * SHA-512 암호화 후 저장
	 */
	public void setPassword(char[] password) {
		try {
			String encryptedPassword = encryptPassword(password);
			lock_element.getChild("password").setText(encryptedPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
