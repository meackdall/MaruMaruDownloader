package fileaccess;

import java.io.*;

/**
 * 경로 지정 객체 정의
 */
public class PathInfo {
	private static final String cur_dir = System.getProperty("user.dir") + File.separator;
	private static final String img_dir = cur_dir + "img" + File.separator;
	private static String default_save_dir = cur_dir + "save" + File.separator;
	private static final String thumbnails_dir = default_save_dir + "thumbnails" + File.separator;
	
	private static final String favicon_path = img_dir + "favicon.png";
	private static final String mainlogo_path = img_dir + "mainlogo.png";
	private static final String thumbnailfailed_path = img_dir + "thumbnailfailed.png";
	private static final String lock_path = img_dir + "lock.png";
	
	private static final String download_list_path = cur_dir + "download_list.xml";
	
	private static final String windows_startup_dir = "C:" + File.separator + "Users" + File.separator + System.getProperty("user.name") 
														+ File.separator + "AppData" + File.separator + "Roaming"
														+File.separator + "Microsoft" + File.separator + "Windows" + File.separator + "Start Menu" 
														+ File.separator + "Programs" + File.separator + "Startup" + File.separator;
	private static final String lnk_path = windows_startup_dir + "다우마루.lnk";
	
	// 현재 디렉토리를 반환하는 메소드
	public static String getCurrentDirectory() {
		return PathInfo.cur_dir;
	}
	
	// 프로그램에 사용된 이미지 디렉토리를 반환하는 메소드
	public static String getImageDirectory() {
		return PathInfo.img_dir;
	}
	
	// 만화가 저장될 경로의 Default root 디렉토리를 반환하는 메소드
	public static String getDefaultSaveDirectory() {
		return PathInfo.default_save_dir;
	}
	
	// 만화의 썸네일 디렉토리를 반환하는 메소드
	public static String getThumbnailsDirectory() {
		return PathInfo.thumbnails_dir;
	}
	
	// favicon 경로를 반환하는 메소드
	public static String getFaviconPath() {
		return PathInfo.favicon_path;
	}
	
	// mainlogo 경로를 반환하는 메소드
	public static String getMainlogoPath() {
		return PathInfo.mainlogo_path;
	}
	
	// thumbnailfailed 경로를 반환하는 메소드
	public static String getThumbnailFailedPath() {
		return PathInfo.thumbnailfailed_path;
	}
	
	// lock 경로를 반환하는 메소드
	public static String getLockPath() {
		return PathInfo.lock_path;
	}
	
	// download_list.xml 파일의 경로를 반환하는 메소드
	public static String getDownloadListPath() {
		return PathInfo.download_list_path;
	}
	
	// 시작 프로그램 경로와 파일 이름을 반환하는 메소드
	public static String getLnkPath() {
		return PathInfo.lnk_path;
	}

}
