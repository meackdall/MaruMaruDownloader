package guiframe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fileaccess.PathInfo;
import fileaccess.XMLHandler;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;

public class PasswordSettingDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private final JPanel contentPanel = new JPanel();
	private JPasswordField passwordfieldNewPassword;
	private JPasswordField passwordfieldNewPasswordConfirm;
	
	private OptionFrame parent;
	private PasswordSettingDialog self = this;
	
	private XMLHandler xmlhandler;
	
	private boolean enablelock;
	private boolean usenewpassword = false;
	
	/**
	 * 옵션을 저장하는 메소드
	 */
	public void Save() {
		xmlhandler.setEnableLock(enablelock);
		xmlhandler.SaveXMLFile();
		JOptionPane.showMessageDialog(self, "저장 완료", "알림", JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Create the dialog.
	 */
	public PasswordSettingDialog(OptionFrame parent) {
		this.parent = parent;
		xmlhandler = new XMLHandler();
		
		int lockcode = xmlhandler.getEnableLock();
		if (lockcode == 1) {
			enablelock = true;
		}
		else if (lockcode == 0) {
			enablelock = false;
		}
		else {
			JOptionPane.showMessageDialog(this, "설정 파일 손상 감지됨", "설정 파일 손상", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		setTitle("프로그램 잠금 설정");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModal(true);
		
		int lockdialog_w = 320;
		int lockdialog_h = 200;
		setBounds(100, 100, 320, 190);
		
		// 선택 프레임 위치를 화면 정중앙으로 설정
		// X 좌표 = 화면 가로에서 선택 프레임의 가로를 뺀 값의 절반
		// Y 좌표 = 화면 세로에서 선택 프레임의 세로를 뺀 값의 절반
		int lockdialog_x = (MainFrame.screenSize.width - lockdialog_w) / 2;
		int lockdialog_y = (MainFrame.screenSize.height - lockdialog_h) / 2;
		setLocation(lockdialog_x, lockdialog_y);
		
		// Favicon 설정
		setIconImage(new ImageIcon(PathInfo.getFaviconPath()).getImage());
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 5));

		JPanel panelLockCheck = new JPanel();
		panelLockCheck.setLayout(new BorderLayout(0, 0));
		contentPanel.add(panelLockCheck, BorderLayout.NORTH);
		
		JCheckBox chkboxLock = new JCheckBox("프로그램 잠그기 사용");
		chkboxLock.setHorizontalAlignment(SwingConstants.LEFT);
		chkboxLock.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent evt) {
				// 프로그램 잠그기 플래그 변경
				if (evt.getStateChange() == 1) {
					enablelock = true;
				}
				else {
					enablelock = false;
				}
			}
		});
		panelLockCheck.add(chkboxLock, BorderLayout.NORTH);
		
		JPanel panelNewPassword = new JPanel();
		contentPanel.add(panelNewPassword, BorderLayout.CENTER);
		panelNewPassword.setLayout(new BorderLayout(0, 0));
		
		JPanel panelNewPasswordInput = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelNewPasswordInput.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		panelNewPassword.add(panelNewPasswordInput, BorderLayout.CENTER);
		
		JLabel lblNewPassword = new JLabel("새 비밀번호");
		panelNewPasswordInput.add(lblNewPassword);
		lblNewPassword.setHorizontalAlignment(JLabel.RIGHT);

		passwordfieldNewPassword = new JPasswordField();
		passwordfieldNewPassword.setColumns(16);
		panelNewPasswordInput.add(passwordfieldNewPassword);
		passwordfieldNewPassword.setHorizontalAlignment(SwingConstants.LEFT);

		JLabel lblNewPasswordConfirm = new JLabel("새 비밀번호 확인");
		panelNewPasswordInput.add(lblNewPasswordConfirm);
		lblNewPasswordConfirm.setHorizontalAlignment(JLabel.RIGHT);

		passwordfieldNewPasswordConfirm = new JPasswordField();
		passwordfieldNewPasswordConfirm.setColumns(16);
		panelNewPasswordInput.add(passwordfieldNewPasswordConfirm);
		passwordfieldNewPasswordConfirm.setHorizontalAlignment(SwingConstants.LEFT);
		
		JCheckBox chkboxNewPassword = new JCheckBox("비밀번호 변경");
		chkboxNewPassword.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent evt) {
				// 비밀번호 변경 체크시 새 비밀번호 필드를 사용할 수 있음
				// 새 비밀번호 사용 플래그를 변경
				if (evt.getStateChange() == 1) {
					passwordfieldNewPassword.setEditable(true);
					passwordfieldNewPasswordConfirm.setEditable(true);
					usenewpassword = true;
				}
				else {
					passwordfieldNewPassword.setEditable(false);
					passwordfieldNewPasswordConfirm.setEditable(false);
					usenewpassword = false;
				}
			}
		});
		chkboxNewPassword.setSelected(true);
		chkboxNewPassword.setSelected(false);
		panelNewPassword.add(chkboxNewPassword, BorderLayout.NORTH);
		
		// 락 다이얼로그를 처음 생성했을 때 XML에서 설정을 불러옴
		if (enablelock) {
			chkboxLock.setSelected(true);
		}
		else {
			chkboxLock.setSelected(false);
		}

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		JButton btnOk = new JButton("확인");
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				if (usenewpassword) {
					if (passwordfieldNewPassword.getPassword().length > 0 && Arrays.equals(passwordfieldNewPassword.getPassword(), passwordfieldNewPasswordConfirm.getPassword())) {
						xmlhandler.setPassword(passwordfieldNewPassword.getPassword());
						Save();
						xmlhandler.close();
						dispose();
					}
					else {
						JOptionPane.showMessageDialog(self, "새 비밀번호가 일치하지 않습니다.", "새 비밀번호 불일치", JOptionPane.ERROR_MESSAGE);
						passwordfieldNewPassword.setText("");
						passwordfieldNewPasswordConfirm.setText("");
						passwordfieldNewPassword.requestFocus();
					}
				}
				else {
					Save();
					xmlhandler.close();
					dispose();
				}
			}
		});
		buttonPane.add(btnOk);
		getRootPane().setDefaultButton(btnOk);

		JButton btnCancle = new JButton("취소");
		btnCancle.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				xmlhandler.close();
				dispose();
			}
		});
		buttonPane.add(btnCancle);

		setVisible(true);

	}
}
