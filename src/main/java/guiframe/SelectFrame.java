package guiframe;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URLEncoder;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.border.LineBorder;

import comicobj.Comics;
import fileaccess.PathInfo;
import fileaccess.XMLHandler;

import java.awt.Color;

/**
 * 만화를 선택하는 프레임 정의
 */
public class SelectFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	protected boolean disposed = false;
	
	private JPanel contentPane;
	private SelectFrame own;
	
	// 위에서부터 가로, 세로, X좌표, Y좌표
	protected int selectframe_w;
	protected int selectframe_h;
	protected int selectframe_x;
	protected int selectframe_y;
	
	protected JScrollPane scrollPaneSelect;
	
	// 만화 목록를 보여줄 요소 핸들러 선언
	// 각 만화 목록 컴포넌트는 패널, 체크박스, 썸네일로 구성
	protected JPanel h_panel[] = new JPanel[30];
	protected JCheckBox h_chkbox[] = new JCheckBox[30];
	protected JLabel h_thumbnail[] = new JLabel[30];
	protected ArrayItemListener h_arrlistenerchkbox[] = new ArrayItemListener[30];
	
	// 추가로 원본 이름과 URL을 저장할 배열 선언
	protected String h_originname[] = new String[30];
	protected String h_url[] = new String[30];
	
	// 페이지 핸들러 선언
	// 페이지를 출력할 라벨, 처음 페이지 버튼, 10 페이지 전 버튼, 이전 페이지 버튼, 
	// 다음 페이지 버튼, 10 페이지 후 버튼, 마지막 페이지 버튼으로 구성
	protected JTextField tfpage;
	protected JButton btnMoveFirst;
	protected JButton btnMovePrev10;
	protected JButton btnMovePrev;
	protected JButton btnMoveNext;
	protected JButton btnMoveNext10;
	protected JButton btnMoveLast;
	
	// 현재 페이지와 마지막 페이지를 저장
	// 처음 페이지는 무조건 1이니 저장할 필요 없음
	protected int curpage = 1;
	protected int lastpage = 1;
	
	// 이전 페이지와 다음 페이지의 URL을 저장
	protected final String first_url = "https://marumaru.in/?r=home&m=bbs&bid=manga";
	protected String last_url;
	protected String prev10_url;
	protected String next10_url;
	protected String prev_url;
	protected String next_url;
	
	// 프레임 조작 핸들러 선언
	// 프레임 항목은 저장 버튼, 취소 버튼으로 구성
	protected JButton btnSave;
	protected JButton btnCancle;
	
	private HashMap<String, Comics> selected_comics_map;
	
	private JPanel panelSearch;
	protected JTextField tfSearch;
	protected JButton btnSearch;
	
	// 페이지 번호가 들어왔을 경우 URL로 변경
	public void LoadComicsListPage(int page_num) {
		String page_list_url = String.format("https://marumaru.in/?c=1/40&p=%d&sort=gid", page_num);
		try {
			LoadComicsListWorker lworker = new LoadComicsListWorker(this, page_list_url, selected_comics_map);
			lworker.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void LoadComicsListPage(String page_list_url) {
		try {
			LoadComicsListWorker lworker = new LoadComicsListWorker(this, page_list_url, selected_comics_map);
			lworker.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 생성자 정의
	public SelectFrame() {
		own = this;
		
		setResizable(false);
		setType(Type.POPUP);
		setTitle("만화 선택");
		
		// "X" 버튼 클릭시 보이지 않게 변경
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
		// 선택 프레임 크기 설정
		selectframe_w = 1280;
		selectframe_h = 720; 
		setBounds(100, 100, selectframe_w, selectframe_h);
		
		// 선택 프레임 위치를 화면 정중앙으로 설정
		// X 좌표 = 화면 가로에서 프레임의 가로를 뺀 값의 절반
		// Y 좌표 = 화면 세로에서 프레임의 세로를 뺀 값의 절반
		selectframe_x = (MainFrame.screenSize.width - selectframe_w) / 2;
		selectframe_y = (MainFrame.screenSize.height - selectframe_h) / 2;
		setLocation(selectframe_x, selectframe_y);
		
		// favicon 설정
		setIconImage(new ImageIcon(PathInfo.getFaviconPath()).getImage());
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panelCloseBtn = new JPanel();
		contentPane.add(panelCloseBtn, BorderLayout.SOUTH);
		panelCloseBtn.setLayout(new FlowLayout(FlowLayout.RIGHT, 15, 5));
		
		btnSave = new JButton("저장");
		// 저장 클릭시 XML 파일에 선택한 만화 목록을 저장
		ActionListener listnerbtnSave = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				XMLHandler xmlhandler = new XMLHandler();
				xmlhandler.SaveSelectedComics(selected_comics_map);
				xmlhandler.SaveXMLFile();
				xmlhandler.close();
				setVisible(false);
			}
		};
		btnSave.addActionListener(listnerbtnSave);
		panelCloseBtn.add(btnSave);
		
		btnCancle = new JButton("취소");
		ActionListener listnerbtnCancle = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		};
		btnCancle.addActionListener(listnerbtnCancle);
		panelCloseBtn.add(btnCancle);
		
		JPanel panelShow = new JPanel();
		contentPane.add(panelShow, BorderLayout.CENTER);
		panelShow.setLayout(new BorderLayout(0, 0));
		
		JPanel panelBtn = new JPanel();
		panelBtn.setLayout(new BorderLayout());
		
		JPanel panelMoveBtn = new JPanel();
		panelBtn.add(panelMoveBtn);
		panelMoveBtn.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 5));
		
		// 첫 페이지 버튼 클릭시 첫 페이지로 이동
		btnMoveFirst = new JButton("첫 페이지");
		ActionListener listnerbtnMoveFirst = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoadComicsListPage(first_url);
			}
		};
		btnMoveFirst.addActionListener(listnerbtnMoveFirst);
		panelMoveBtn.add(btnMoveFirst);
		
		// ◀◀ 버튼 클릭시 10 페이지 전으로 이동
		btnMovePrev10 = new JButton("◀◀");
		ActionListener listnerbtnMovePrev10 = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoadComicsListPage(prev10_url);
			}
		};
		btnMovePrev10.addActionListener(listnerbtnMovePrev10);
		panelMoveBtn.add(btnMovePrev10);
		
		// ◀ 버튼 클릭시 이전 페이지로 이동
		btnMovePrev = new JButton("◀");
		ActionListener listnerbtnMovePrev = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoadComicsListPage(prev_url);
			}
		};
		btnMovePrev.addActionListener(listnerbtnMovePrev);
		panelMoveBtn.add(btnMovePrev);
		
		
		tfpage = new JTextField(3);
		tfpage.setText("1");
		tfpage.setHorizontalAlignment(JTextField.CENTER);
		tfpage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 입력 값이 숫자 1자리 이상일 경우에만 페이지 로드
				Pattern pagenum = Pattern.compile("^[0-9]{1,}$");
				Matcher matcher = pagenum.matcher(tfpage.getText());
				if (matcher.find()) {
					int intendpage = Integer.parseInt(tfpage.getText());
					// 1보다 작을 경우 1페이지로 이동
					// 마지막 페이지보다 클 경우 마지막 페이지로 이동
					if (intendpage < 1) {
						intendpage = 1;
					}
					else if (intendpage > lastpage) {
						intendpage = lastpage;
					}
					
					// 현재 페이지와 같다면 로드하지 않고 텍스트 값만 변경
					if (intendpage == curpage) {
						tfpage.setText(Integer.toString(intendpage));
					}
					else {
						LoadComicsListPage(intendpage);
					}
				}
				else {
					JOptionPane.showMessageDialog(own, "1~999까지의 숫자를 입력해야 합니다.", "오류", JOptionPane.WARNING_MESSAGE);
					tfpage.setText(Integer.toString(curpage));
				}
			}
		});
		panelMoveBtn.add(tfpage);
		
		// ▶ 버튼 클릭시 다음 페이지로 이동
		btnMoveNext = new JButton("▶");
		ActionListener listnerbtnMoveNext = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoadComicsListPage(next_url);
			}
		};
		btnMoveNext.addActionListener(listnerbtnMoveNext);
		panelMoveBtn.add(btnMoveNext);
		
		// ▶▶ 버튼 클릭시 10 페이지 후로 이동
		btnMoveNext10 = new JButton("▶▶");
		ActionListener listnerbtnMoveNext10 = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoadComicsListPage(next10_url);
			}
		};
		btnMoveNext10.addActionListener(listnerbtnMoveNext10);
		panelMoveBtn.add(btnMoveNext10);
		
		// 끝 페이지 버튼 클릭시 끝 페이지로 이동
		btnMoveLast = new JButton("끝 페이지");
		ActionListener listnerbtnMoveLast = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				LoadComicsListPage(last_url);
			}
		};
		btnMoveLast.addActionListener(listnerbtnMoveLast);
		panelMoveBtn.add(btnMoveLast);
		
		panelBtn.add(panelMoveBtn, BorderLayout.CENTER);
		
		scrollPaneSelect = new JScrollPane();
		scrollPaneSelect.getVerticalScrollBar().setUnitIncrement(25);
		panelShow.add(scrollPaneSelect, BorderLayout.CENTER);
		panelShow.add(panelBtn, BorderLayout.SOUTH);
		
		panelSearch = new JPanel();
		panelBtn.add(panelSearch, BorderLayout.SOUTH);
		panelSearch.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		ActionListener searchActionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				String searchKeyWord = tfSearch.getText();
				if (searchKeyWord.trim().length() < 1) {
					JOptionPane.showMessageDialog(null, "검색할 단어를 입력해야 합니다.", "검색 단어 없음", JOptionPane.ERROR_MESSAGE);
				}
				else {
					try {
						String searchURL = String.format("http://marumaru.in/?r=home&mod=search&keyword=%s&x=0&y=0", 
															URLEncoder.encode(searchKeyWord, "UTF-8"));
						LoadComicsListPage(searchURL);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		};
		
		tfSearch = new JTextField();
		tfSearch.setColumns(15);
		tfSearch.addActionListener(searchActionListener);
		panelSearch.add(tfSearch);
		
		btnSearch = new JButton("검색");
		btnSearch.addActionListener(searchActionListener);
		panelSearch.add(btnSearch);
		
		JPanel panelSelect = new JPanel();
		scrollPaneSelect.setViewportView(panelSelect);
		panelSelect.setLayout(new GridLayout(0, 6, 5, 10));
		
		// 30개의 만화 패널 생성
		int i = 0;
		for (i = 0; i < 30; i++) {
			// 만화 패널 생성
			h_panel[i] = new JPanel();
			h_panel[i].setBorder(new LineBorder(new Color(0, 0, 0)));
			panelSelect.add(h_panel[i]);
			h_panel[i].setLayout(new BorderLayout());
			
			// 체크박스, 아이템 리스너 생성
			h_chkbox[i] = new JCheckBox(h_originname[i]);
			h_arrlistenerchkbox[i] = new ArrayItemListener(i);
			h_chkbox[i].addItemListener(h_arrlistenerchkbox[i]);
			h_panel[i].add(h_chkbox[i], BorderLayout.CENTER);
			
			// 썸네일 생성
			h_thumbnail[i] = new JLabel();
			h_panel[i].add(h_thumbnail[i], BorderLayout.NORTH);
			
		}	
		
		setVisible(true);
		
		// XML에서 선택한 만화 목록을 로드
		XMLHandler xmlhandler = new XMLHandler();
		selected_comics_map = xmlhandler.LoadSelectedComicsList();
		xmlhandler.close();
		
		// 바로 1페이지를 로드
		LoadComicsListPage(first_url);
	}
	
	/**
	 *  ItemListener 인터페이스를 상속받아 원하는대로 조작하도록 만든 클래스
	 *  index 정보와 자동변경 여부를 저장
	 */
	public class ArrayItemListener implements ItemListener {
		protected int index = 0;
		protected boolean autochange = false;
		
		// 생성자에 반드시 Index 필요
		public ArrayItemListener(int index) {
			this.index = index;
		}
		
		// 자동 변경을 셋팅하는 메소드
		public void setAutochange(boolean autochanged) {
			this.autochange = autochanged;
		}
		
		@Override
		public void itemStateChanged(ItemEvent itemEvent) {
			// 페이지가 이동할 경우 체크박스가 변하므로 사용자가 직접 Select/UnSelect한건지 아닌지 판단해야함
			// 사용자가 직접 체크/해제를 발생시킨 경우에만 리스트에 추가하거나 제거
			// .setSelect(boolean)으로 변경할 경우 위아래에 setAutochange(true) setAutochange(false)를 삽입
			if (!autochange) {
				// getStateChange == 1 -> checked
				// getStateChange != 1 -> unchecked
				if (itemEvent.getStateChange() == 1) {
					selected_comics_map.put(h_originname[index], new Comics(h_originname[index], h_url[index]));
				}
				else {
					selected_comics_map.remove(h_originname[index]);
				}
			}
		}
	}
	
}
