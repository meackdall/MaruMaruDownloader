package guiframe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fileaccess.PathInfo;
import fileaccess.XMLHandler;

public class OptionFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private OptionFrame self = this;
	
	protected int optionframe_w;
	protected int optionframe_h;
	protected int optionframe_x;
	protected int optionframe_y;
	
	XMLHandler xmlhandler;
	
	private String save_path;
	
	private JCheckBox chkboxCookie;
	private JTextField tfPhpsession;
	
	private boolean enablecookie = false;
	private String cookie = null;
	private boolean enablestartup = false;
	
	private PasswordSettingDialog passwordsettingdialog = null;
	
	/**
	 * 시작 프로그램에 등록하는 메소드
	 */
	public void setStartupProgram(boolean startup) {
		File startupfile = new File(PathInfo.getLnkPath());
		if (startup) {
			if (!startupfile.isFile()) {
				try {
					FileInputStream startupin = new FileInputStream(PathInfo.getCurrentDirectory() + "다우마루.lnk");
					FileOutputStream startupout = new FileOutputStream(startupfile);
					byte[] readByte = new byte[4096];
					while (startupin.read(readByte) != -1) {
						startupout.write(readByte);
					}
					startupin.close();
					startupout.close();
				} catch ( Exception e) {
					e.printStackTrace();
				}
			}
		}
		else {
			startupfile.delete();
		}
	}
	
	public OptionFrame() {
		// XML에서 만화가 저장될 위치와 각종 설정들을 로드
		xmlhandler = new XMLHandler();
		save_path = xmlhandler.getSavePath();
		enablecookie = xmlhandler.getCookieEnabled();
		cookie = xmlhandler.getCookie();
		enablestartup = xmlhandler.getStartup();
		xmlhandler.close();
		
		setTitle("옵션");
		setType(Type.POPUP);
		// "X" 버튼 클릭시 보이지 않게 변경
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setResizable(false);
		
		// Favicon 설정
		setIconImage(new ImageIcon(PathInfo.getFaviconPath()).getImage());
		
		// 옵션 프레임 크기 설정
		optionframe_w = 500;
		optionframe_h = 300; 
		
		// 선택 프레임 위치를 화면 정중앙으로 설정
		// X 좌표 = 화면 가로에서 옵션 프레임의 가로를 뺀 값의 절반
		// Y 좌표 = 화면 세로에서 옵션 프레임의 세로를 뺀 값의 절반
		optionframe_x = (MainFrame.screenSize.width - optionframe_w) / 2;
		optionframe_y = (MainFrame.screenSize.height - optionframe_h) / 2;
		setBounds(optionframe_x, optionframe_y, optionframe_w, optionframe_h);
		
		JPanel optionpanel = new JPanel();
		optionpanel.setBounds(0, 0, optionframe_w, optionframe_h);
		optionpanel.setLayout(null);
		
		int pathlabel_x = 30;
		int pathlabel_y = 20;
		int pathlabel_w = 100;
		int pathlabel_h = 25;
		
		JPanel pathlabelpanel = new JPanel();
		pathlabelpanel.setBounds(pathlabel_x, pathlabel_y, pathlabel_w, pathlabel_h);
		JLabel pathlabel = new JLabel("저장 위치");
		pathlabelpanel.add(pathlabel);
		
		int pathtext_x = pathlabel_x;
		int pathtext_y = pathlabel_y + 25;
		int pathtext_w = 300;
		int pathtext_h = 35;
		
		JPanel panelPathtext = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelPathtext.setBounds(pathtext_x, pathtext_y, pathtext_w, pathtext_h);
		JTextField pathtext = new JTextField(25);
		pathtext.setEditable(false);
		pathtext.setText(new File(save_path).getAbsolutePath());
		panelPathtext.add(pathtext);
		
		int pathbtn_x = pathtext_x + pathtext_w;
		int pathbtn_y = pathtext_y;
		int pathbtn_w = 100;
		int pathbtn_h = 30;
		
		// 저장 경로 옵션 패널 정의
		JPanel pathbtnpanel = new JPanel();
		pathbtnpanel.setBounds(pathbtn_x, pathbtn_y, pathbtn_w, pathbtn_h);
		JButton btnPath = new JButton("위치 선택...");
		btnPath.setPreferredSize(new Dimension(100, 23));
		btnPath.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				// 파일 선택 화면 호출, 디렉토리만 보여줌
				// 현재 디렉토리 위치를 XML에서 읽어온 디렉토리로 설정
				JFileChooser pathchooser = new JFileChooser();
				pathchooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				pathchooser.setCurrentDirectory(new File(save_path));
				int rVal = pathchooser.showOpenDialog(OptionFrame.this);
				if (rVal == JFileChooser.APPROVE_OPTION) {
					save_path = pathchooser.getCurrentDirectory().getAbsolutePath() + File.separator + pathchooser.getSelectedFile().getName() + File.separator;
					pathtext.setText(save_path);
				}
			}
		});
		pathbtnpanel.add(btnPath);
		
		// 쿠키 값 옵션 패널 정의
		JPanel panelCookie = new JPanel();
		panelCookie.setBounds(30, 80, 400, 50);
		panelCookie.setLayout(new BorderLayout(5, 0));
		chkboxCookie = new JCheckBox("비밀번호 우회 사용");
		chkboxCookie.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent evt) {
				if (evt.getStateChange() == 1) {
					enablecookie = true;
					tfPhpsession.setEditable(true);
				}
				else {
					enablecookie = false;
					tfPhpsession.setEditable(false);
				}
			}
		});
		
		JLabel lblPhpsession = new JLabel("PHPSESSION");
		lblPhpsession.setHorizontalAlignment(JLabel.RIGHT);
		
		JPanel panelCookieText = new JPanel();
		panelCookieText.setLayout(new BoxLayout(panelCookieText, BoxLayout.Y_AXIS));
		tfPhpsession = new JTextField(30);
		panelCookieText.add(tfPhpsession);
		panelCookie.add(chkboxCookie, BorderLayout.NORTH);
		panelCookie.add(lblPhpsession, BorderLayout.WEST);
		panelCookie.add(panelCookieText, BorderLayout.CENTER);
		
		// 옵션 프레임을 처음 생성했을 때 XML에서 설정을 불러옴
		if (enablecookie) {
			chkboxCookie.setSelected(true);
			tfPhpsession.setText(cookie);
		}
		else {
			// 단순히 false만 하면 처음 옵션에 진입했을 때 텍스트필드가 액션을 감지하지 못함
			// 이를 방지하기위해 true로 바꿨다가 빠르게 false로 변경
			chkboxCookie.setSelected(true);
			chkboxCookie.setSelected(false);
		}
		
		// 자동 시작 옵션 패널 정의
		JPanel panelStartup = new JPanel();
		panelStartup.setBounds(30, 140, 135, 50);
		JCheckBox chkboxStartup = new JCheckBox("만화 자동 업데이트");
		chkboxStartup.setSelected(enablestartup);
		chkboxStartup.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent evt) {
				if (evt.getStateChange() == 1) {
					enablestartup = true;
				}
				else {
					enablestartup = false;
				}
			}
		});
		panelStartup.add(chkboxStartup);
		
		// 프로그램 잠그기 옵션 패널 정의
		JPanel panelLock = new JPanel();
		panelLock.setBounds(300, 145, 150, 25);
		panelLock.setLayout(new BorderLayout());
		JButton btnLock = new JButton("프로그램 잠금 설정");
		btnLock.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (passwordsettingdialog == null || !passwordsettingdialog.isDisplayable()) {
					// 프로그램 잠금 사용 중일경우 LockDialog 생성
					if (xmlhandler.getEnableLock() == 1) {
						LockDialog lockdialog = new LockDialog();
						if (lockdialog.isPassed()) {
							passwordsettingdialog = new PasswordSettingDialog(self);
							lockdialog.dispose();
						}
					}
					else {
						passwordsettingdialog = new PasswordSettingDialog(self);
					}
				}
				else {
					passwordsettingdialog.toFront();
				}
			}
		});
		panelLock.add(btnLock, BorderLayout.WEST);
		
		
		// 버튼 패널 정의
		JPanel panelButton = new JPanel(new GridLayout(1, 2, 50, 0));
		panelButton.setBounds(140, 220, 220, 30);
		JButton btnSave = new JButton("저장");
		// 저장 클릭시 XML에 만화의 저장 위치가 저장
		// 자동 업데이트 사용시 시작 프로그램에 등록
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (enablecookie && tfPhpsession.getText().trim().length() < 1) {
					JOptionPane.showMessageDialog(self, "쿠키 값을 입력해야 합니다.", "실패", JOptionPane.ERROR_MESSAGE);
				} else {
					xmlhandler.setSavePath(save_path);
					xmlhandler.EnableCookie(enablecookie);
					xmlhandler.setCookie(tfPhpsession.getText());
					xmlhandler.setStartup(enablestartup);
					setStartupProgram(enablestartup);
					xmlhandler.SaveXMLFile();
					xmlhandler.close();
					setVisible(false);
				}
			}
		});
		
		JButton btnCancle = new JButton("취소");
		btnCancle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				dispose();
			}
		});
		panelButton.add(btnSave);
		panelButton.add(btnCancle);
		
		optionpanel.add(pathlabelpanel);
		optionpanel.add(panelPathtext);
		optionpanel.add(pathbtnpanel);
		optionpanel.add(panelButton);
		optionpanel.add(panelCookie);
		optionpanel.add(panelStartup);
		optionpanel.add(panelLock);
		add(optionpanel);
		
		setVisible(true);
	}
}
