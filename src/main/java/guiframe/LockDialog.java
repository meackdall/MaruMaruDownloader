package guiframe;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fileaccess.PathInfo;
import fileaccess.XMLHandler;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

public class LockDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private LockDialog self = this;
	
	private final JPanel contentPanel = new JPanel();
	
	private JPasswordField passwordfieldLock;
	private XMLHandler xmlhandler;
	
	private boolean isPassed = false;
	
	public boolean isPassed() {
		return isPassed;
	}

	/**
	 * Create the dialog.
	 */
	public LockDialog() {
		xmlhandler = new XMLHandler();
		setTitle("프로그램 잠김");
		
		// 옵션 프레임 크기 설정
		int lockdialog_w = 250;
		int lockdialog_h = 350; 
		
		// 선택 프레임 위치를 화면 정중앙으로 설정
		// X 좌표 = 화면 가로에서 옵션 프레임의 가로를 뺀 값의 절반
		// Y 좌표 = 화면 세로에서 옵션 프레임의 세로를 뺀 값의 절반
		int lockdialog_x = (MainFrame.screenSize.width - lockdialog_w) / 2;
		int lockdialog_y = (MainFrame.screenSize.height - lockdialog_h) / 2;
		setBounds(lockdialog_x, lockdialog_y, lockdialog_w, lockdialog_h);
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		setModal(true);
		
		// Favicon 설정
		setIconImage(new ImageIcon(PathInfo.getFaviconPath()).getImage());
		
		JLabel lblLockImage = new JLabel(XMLHandler.GetResizedImage(new File(PathInfo.getLockPath()), 210, 210));
		lblLockImage.setBounds(12, 10, 210, 210);
		contentPanel.add(lblLockImage);
		
		passwordfieldLock = new JPasswordField();
		passwordfieldLock.setColumns(16);
		passwordfieldLock.setBounds(30, 250, 175, 20);
		contentPanel.add(passwordfieldLock);
		
		JLabel lblShowMessage = new JLabel("프로그램 잠김");
		lblShowMessage.setHorizontalAlignment(SwingConstants.CENTER);
		lblShowMessage.setBounds(72, 225, 90, 15);
		contentPanel.add(lblShowMessage);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);

		JButton btnUnlock = new JButton("입력");
		btnUnlock.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				if (passwordfieldLock.getPassword().length > 0) {
					if (xmlhandler.isCorrectPassword(passwordfieldLock.getPassword())) {
						isPassed = true;
						xmlhandler.close();
						dispose();
					}
					else {
						JOptionPane.showMessageDialog(self, "비밀번호 불일치", "비밀번호 불일치", JOptionPane.ERROR_MESSAGE);
						passwordfieldLock.setText("");
						passwordfieldLock.requestFocus();
					}
				}
				else {
					JOptionPane.showMessageDialog(self, "비밀번호를 입력해야 합니다.", "비밀번호 미입력", JOptionPane.ERROR_MESSAGE);
					passwordfieldLock.requestFocus();
				}
			}
		}); 
		buttonPane.add(btnUnlock);
		getRootPane().setDefaultButton(btnUnlock);

		JButton btnCancle = new JButton("취소");
		btnCancle.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				xmlhandler.close();
				dispose();
			}
		});
		buttonPane.add(btnCancle);
		
		setVisible(true);
	}
}
