package guiframe;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fileaccess.PathInfo;
import fileaccess.XMLHandler;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import javax.swing.JTextPane;

/**
 * 메인 프레임 정의
 */
public class MainFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	protected boolean autostarted = false;
	protected JFrame self;
	
	private JPanel contentPane;
	protected SelectFrame selectframe = null;
	protected OptionFrame optionframe = null;
	protected DownloadFrame downloadframe = null;
	
	protected static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();;
	
	// 위에서부터 가로, 세로, X좌표, Y좌표
	protected int mainframe_w;
	protected int mainframe_h;
	protected int mainframe_x;
	protected int mainframe_y;
	
	protected ActionListener actionlistenerExit;
	
	private XMLHandler xmlhandler;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				boolean autostarted = false;
				try {
					XMLHandler xmlhandler = new XMLHandler();
					xmlhandler.checkPath();
					xmlhandler.copyLnk();
					xmlhandler.SaveXMLFile();
					xmlhandler.close();
					if (args.length > 0 && args[0].equals("--auto")) {
						autostarted = true;
					}
					MainFrame frame = new MainFrame(autostarted);
					frame.setVisible(true);
					if (autostarted) {
						frame.setVisible(false);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	// 생성자 정의
	public MainFrame(boolean autostarted) {
		this.autostarted = autostarted;
		xmlhandler = new XMLHandler();
		
		self = this;
		setTitle("다우마루");
		setResizable(false);
		// "X" 버튼 클릭시 모든 관련 프레임 종료
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// 메인 프레임 크기 설정
		mainframe_w = 420;
		mainframe_h = 300; 
		setBounds(100, 100, mainframe_w, mainframe_h);
		
		// 메인 프레임 위치를 화면 정중앙으로 설정
		// X 좌표 = 화면 가로에서 프레임의 가로를 뺀 값의 절반
		// Y 좌표 = 화면 세로에서 프레임의 세로를 뺀 값의 절반
		mainframe_x = (screenSize.width - mainframe_w) / 2;
		mainframe_y = (screenSize.height - mainframe_h) / 2;
		setLocation(mainframe_x, mainframe_y);
		
		// Favicon 설정
		setIconImage(new ImageIcon(PathInfo.getFaviconPath()).getImage());
		
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// 다운로드 시작 버튼
		// 다운로드 시작 버튼 클릭시 다운로드 시작 창 생성
		// 이미 다운로드 시작 창이 있다면 포커스를 가져옴
		JButton btnStartDownload = new JButton("다운로드 시작");
		btnStartDownload.setBounds(280, 20, 120, 40);
		btnStartDownload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (downloadframe == null || !downloadframe.isDisplayable()) {
					// 프로그램 잠금이 사용중일 경우 LockDialog 생성
					if (xmlhandler.getEnableLock() == 1) {
						LockDialog lockdialog = new LockDialog();
						if (lockdialog.isPassed()) {
							downloadframe = new DownloadFrame(self);
							lockdialog.dispose();
						}
					}
					else {
						downloadframe = new DownloadFrame(self);
					}
				}
				else {
					downloadframe.toFront();
				}
			}
		});
		contentPane.add(btnStartDownload);
		
		// 만화 선택 버튼
		// 만화 선택 버튼 클릭시 만화 선택창 생성
		// 이미 만화 선택창이 있다면 포커스를 가져옴
		JButton btnSelect = new JButton("만화 선택");
		btnSelect.setBounds(280, 80, 120, 40);
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (selectframe == null || selectframe.disposed) {
					// 프로그램 잠금을 사용중일 경우 LockDialog 생성
					if (xmlhandler.getEnableLock() == 1) {
						LockDialog lockdialog = new LockDialog();
						if (lockdialog.isPassed()) {
							selectframe = new SelectFrame();
							lockdialog.dispose();
						}
					}
					else {
						selectframe = new SelectFrame();
					}
				}
				else {
					if (xmlhandler.getEnableLock() == 1) {
						LockDialog lockdialog = new LockDialog();
						if (lockdialog.isPassed()) {
							selectframe.setVisible(true);
							selectframe.toFront();
							lockdialog.dispose();
						}
					}
					else {
						selectframe.setVisible(true);
						selectframe.toFront();
					}
				}
			}
		});
		contentPane.add(btnSelect);
		
		// 옵션 버튼
		// 옵션 버튼 클릭시 옵션창 생성
		// 이미 옵션창이 있다면 포커스를 가져옴
		JButton btnOption = new JButton("옵션");
		btnOption.setBounds(280, 140, 120, 40);
		btnOption.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (optionframe == null || !optionframe.isDisplayable()) {
					optionframe = new OptionFrame();
				}
				else {
					optionframe.setVisible(true);
					optionframe.toFront();
				}
			}
		});
		contentPane.add(btnOption);
		
		// 종료 버튼
		// 종료 버튼 클릭시 모든 프레임 종료
		// 중복 실행 방지를 위해 걸었던 Lock 해제
		JButton btnExit = new JButton("종료");
		btnExit.setBounds(280, 200, 120, 40);
		actionlistenerExit = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		};
		btnExit.addActionListener(actionlistenerExit);
		contentPane.add(btnExit);
		
		// 제작자 명시
		JLabel lblCreator = new JLabel("Create by Proof");
		lblCreator.setFont(new Font("굴림", Font.BOLD, 12));
		lblCreator.setBounds(5, 257, 103, 15);
		contentPane.add(lblCreator);
		
		// 메인 로고를 부착할 패널 정의
		JPanel panelMain = new JPanel();
		panelMain.setBackground(Color.WHITE);
		panelMain.setBounds(12, 10, 256, 220);
		contentPane.add(panelMain);
		panelMain.setLayout(new BorderLayout(15, 15));
		
		// 메인 로고 라벨에 메인 로고 추가
		ImageIcon mainlogo = new ImageIcon(PathInfo.getMainlogoPath());
		JLabel lblMainlogo = new JLabel(mainlogo);
		panelMain.add(lblMainlogo, BorderLayout.WEST);
		
		// 메인 타이틀 추가
		JLabel lblMaintitle = new JLabel("다우마루 사용법");
		lblMaintitle.setFont(new Font("굴림", Font.BOLD, 12));
		panelMain.add(lblMaintitle, BorderLayout.CENTER);
		
		// 사용법 추가
		JTextPane textPaneUsage = new JTextPane();
		textPaneUsage.setBackground(Color.WHITE);
		textPaneUsage.setText(
				  "1. 옵션에서 만화를 저장할 위치를 선택\r\n\r\n"
				+ "2. 만화 선택에서 다운로드할 만화를 선택\r\n\r\n"
				+ "3. 다운로드 시작을 눌러 만화를 다운로드\r\n\r\n"
				+ "▶ 자동 업데이트는 옵션에서 설정");
		textPaneUsage.setEditable(false);
		panelMain.add(textPaneUsage, BorderLayout.SOUTH);
		
		// 자동 시작일 경우
		if (this.autostarted) {
			downloadframe = new DownloadFrame(self);
		}
	}
}
