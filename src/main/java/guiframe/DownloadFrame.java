package guiframe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import comicobj.Comics;
import fileaccess.PathInfo;
import fileaccess.XMLHandler;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class DownloadFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	protected MainFrame parent;
	
	private final JPanel contentPanel = new JPanel();
	
	private int downloaddialog_w;
	private int downloaddialog_h;
	
	private HashMap<String, Comics> selected_comics_map;
	
	private int comics_list_size;
	private XMLHandler xmlhandler = new XMLHandler();
	
	protected JButton btnOK;
	protected JButton btnOpenDirectory;
	protected JButton btnCancle;
	
	protected JLabel lblstatus[];
	protected JLabel lblname[];
	
	// 상태 정보에 표시할 상태
	// StateYet = 아직 다운로드 하지 않음
	// StateNormal = 정상적으로 다운로드 완료
	// StateLock = 패스워드가 걸린 만화
	// StateError = 다운로드 도중 에러가 발생
	// StateDownloading = 현재 다운로드 중
	protected static final String StateYet = "              ";
	protected static final String StateNormal = "    ✔      ";
	protected static final String StateLock = "    🔒     ";
	protected static final String StateError = "    ⊘      ";
	protected static final String StateDownloading = "     →     ";
	
	public void StartDownload() {
		setLocation((MainFrame.screenSize.width / 2) - downloaddialog_w, 
					(MainFrame.screenSize.height - downloaddialog_h) / 2);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		btnOK.setText("다운로드 중...");
		btnOK.setEnabled(false);
		btnCancle.setEnabled(false);
		DownloadWorker downloadworker = new DownloadWorker(this, selected_comics_map);
		downloadworker.execute();
	}

	/**
	 *  - 다운로드 다이얼로그 구조 -
	 *  +-----------------------+
	 *  |   DownloadListTitle   |
	 *  +------------+----------+
	 *  | StatusInfo | NameInfo |
	 *  +------------+----------+
	 *  |     ✔            | Comics1  | <- 완료된 목록
	 *  |     🔒      | Comics2  | <- 패스워드가 걸린 만화
	 *  |     →      | Comics3  | <- 현재 다운로드 받는 중
	 *  |            | Comics4  |
	 *  +------------+----------+
	 */
	public DownloadFrame(JFrame parent) {
		this.parent = (MainFrame) parent;
		setTitle("다운로드 목록");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		downloaddialog_w = 500;
		downloaddialog_h = 350;
		
		setBounds(100, 100, downloaddialog_w, downloaddialog_h);
		setResizable(false);
		
		// 다운로드 목록창 위치를 설정
		// X 좌표 = 화면 가로에서 프레임의 가로를 뺀 값의 절반
		// Y 좌표 = 화면 세로에서 프레임의 세로를 뺀 값의 절반
		int downloaddialog_x = (MainFrame.screenSize.width - downloaddialog_w) / 2;
		int downloaddialog_y = (MainFrame.screenSize.height - downloaddialog_h) / 2;
		setLocation(downloaddialog_x, downloaddialog_y);
		
		
		// Favicon 설정
		setIconImage(new ImageIcon(PathInfo.getFaviconPath()).getImage());
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		
		// 타이틀 패널
		JPanel panelTitle = new JPanel();
		contentPanel.add(panelTitle, BorderLayout.NORTH);
		panelTitle.setLayout(new BorderLayout(0, 0));

		// 메인 타이틀
		JLabel lblDownloadListTitle = new JLabel("다운로드 목록");
		lblDownloadListTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblDownloadListTitle.setFont(new Font("굴림", Font.BOLD, 15));
		panelTitle.add(lblDownloadListTitle, BorderLayout.NORTH);

		// 상태 정보
		JLabel lblStateInfo = new JLabel("  상태  ");
		lblStateInfo.setBackground(Color.WHITE);
		lblStateInfo.setOpaque(true);
		lblStateInfo.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblStateInfo.setHorizontalAlignment(SwingConstants.CENTER);
		panelTitle.add(lblStateInfo, BorderLayout.WEST);

		// 만화 제목 정보
		JLabel lblNameInfo = new JLabel("만화 제목");
		lblNameInfo.setBackground(Color.WHITE);
		lblNameInfo.setOpaque(true);
		lblNameInfo.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblNameInfo.setHorizontalAlignment(SwingConstants.CENTER);
		panelTitle.add(lblNameInfo, BorderLayout.CENTER);

		JScrollPane scrollPaneShowList = new JScrollPane();
		scrollPaneShowList.getVerticalScrollBar().setUnitIncrement(25);
		contentPanel.add(scrollPaneShowList, BorderLayout.CENTER);

		// 상태 라벨
		JPanel panelShowStatus = new JPanel();
		panelShowStatus.setBackground(Color.WHITE);
		scrollPaneShowList.setRowHeaderView(panelShowStatus);
		panelShowStatus.setLayout(new BoxLayout(panelShowStatus, BoxLayout.Y_AXIS));

		// 만화 이름 라벨
		JPanel panelShowName = new JPanel();
		panelShowName.setBackground(Color.WHITE);
		scrollPaneShowList.setViewportView(panelShowName);
		panelShowName.setLayout(new BoxLayout(panelShowName, BoxLayout.Y_AXIS));

		// 버튼 패널
		JPanel paneButton = new JPanel();
		paneButton.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(paneButton, BorderLayout.SOUTH);

		btnOK = new JButton("시작");
		btnOK.setActionCommand("OK");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				StartDownload();
			}
		});
		paneButton.add(btnOK);
		getRootPane().setDefaultButton(btnOK);
		
		// 만화가 저장된 위치를 여는 버튼 
		btnOpenDirectory = new JButton("저장 폴더 열기");
		btnOpenDirectory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				XMLHandler xmlhandler = new XMLHandler();
				try {
					Desktop.getDesktop().open(new File(xmlhandler.getSavePath()));
				} catch (IOException e) {
					e.printStackTrace();
				}
				xmlhandler.close();
			}
		});
		paneButton.add(btnOpenDirectory);

		btnCancle = new JButton("취소");
		btnCancle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				dispose();
			}
		});
		btnCancle.setActionCommand("Cancel");
		paneButton.add(btnCancle);
		
		// XML 핸들러에서 선택한 만화 목록을 로드
		selected_comics_map = xmlhandler.LoadSelectedComicsList();
		comics_list_size = selected_comics_map.size();
		
		// 상태와 만화 제목 라벨을 생성하고 각 항목을 업데이트
		lblstatus = new JLabel[comics_list_size];
		lblname = new JLabel[comics_list_size];
		
		Iterator<String> selected_comics_map_iter = selected_comics_map.keySet().iterator();
		
		int i = 0;
		while (selected_comics_map_iter.hasNext()) {
			String selected_comics_name = selected_comics_map_iter.next();
			
			// 상태는 기본적으로 공백 문자열
			lblstatus[i] = new JLabel(StateYet);
			lblstatus[i].setHorizontalAlignment(JLabel.CENTER);
			panelShowStatus.add(lblstatus[i]);
			panelShowStatus.add(Box.createRigidArea(new Dimension(0, 1)));
			
			// 제목 생성, 가운데 정렬하지 않음
			lblname[i] = new JLabel(selected_comics_name);
			panelShowName.add(lblname[i]);
			panelShowName.add(Box.createRigidArea(new Dimension(0, 1)));
			i++;
		}
		
		// 선택된 만화 목록이 없을경우 시작할 수 없음
		if (selected_comics_map.size() == 0) {
			btnOK.setEnabled(false);
		}
		
		// 자동 실행일 경우
		if (this.parent.autostarted) {
			StartDownload();
		}
		else {
			setVisible(true);
		}
	}

}
