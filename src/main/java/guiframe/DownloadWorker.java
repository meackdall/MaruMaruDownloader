package guiframe;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import org.jdom2.Element;

import comicobj.Comics;
import comicobj.Episode;
import fileaccess.PathInfo;
import fileaccess.XMLHandler;

public class DownloadWorker extends SwingWorker<Object, Integer>{
	// 이 프레임을 부르는 상위 프레임의 객체 저장
	// 진행 경과를 보여주는 프레임 저장
	private DownloadFrame parent;
	private JFrame progressframe;
	
	// 팝업 메뉴를 포함한 트레이 아이콘을 정의할 요소
	private PopupMenu traypopup;
	private SystemTray systemtray;
	private TrayIcon trayicon;
	private MenuItem mitemCancle;
	private MenuItem mitemExit;
	
	// 진행 경과 막대, 썸네일과 에피소드 이름을 표시할 라벨 정의
	private JProgressBar progressbar;
	private JLabel lblthumbnail;
	private JLabel lblname;
	
	private HashMap<String, Comics> selected_comics_map;
	
	private XMLHandler xmlhandler;
	
	private LockDialog lockdialog = null;
	private boolean lockdialog_active = false;
	
	// 썸네일을 패널 크기에 맞도록 변경해 불러오는 메소드
	public ImageIcon GetThumbnail(String comics_name) {
		XMLHandler xmlhandler = new XMLHandler();
		List<Element> thumbnail_list = xmlhandler.LoadThumbnailPathList();
		
		for (Element thumbnail : thumbnail_list) {
			if (thumbnail.getText().equals(xmlhandler.getThumbnailPath() + "thumb_" + XMLHandler.ReplaceInvalidChar(comics_name) + ".jpg")) {
				File thumbnail_file = new File(thumbnail.getText());
				return XMLHandler.GetResizedImage(thumbnail_file, 350, 490);
			}
		}
		return null;
	}
	
	public void ChangeFrameToTray(boolean autostarted) {
		progressframe.setVisible(false);
		parent.setVisible(false);
		parent.parent.setVisible(false);
		try {
			systemtray.add(trayicon);
			String traymsg = null;
			if (autostarted) {
				traymsg = "만화 업데이트 중...";
			}
			else {
				traymsg = "다운로드 중...";
			}
			trayicon.displayMessage(null, traymsg, TrayIcon.MessageType.INFO);
		} catch (AWTException awtex) {
			awtex.printStackTrace();
		}
	}
	
	/**
	 * 트레이 아이콘에서 프레임으로 변환하는 메소드
	 */
	public void ChangeTrayToFrame() {
		if (xmlhandler.getEnableLock() == 1) {
			// 여러 개의 lock dialog를 생성하지 않기 위해
			// 현재 lock dialog가 띄워져 있는지 lockdialog_active로 확인
			// Modal이 true이므로 LockDialog가 종료되기 전까지 lockdialog 변수에 할당되지 않음
			if (lockdialog_active == false) {
				lockdialog_active = true;
				lockdialog = new LockDialog();
				lockdialog_active = false;
				
				// 통과했을 경우에만 원래 프레임으로 변환
				if (lockdialog.isPassed()) {
					systemtray.remove(trayicon);
					parent.parent.setVisible(true);
					parent.setVisible(true);
					// 다운로드가 완료되었을 경우 경과 프레임은 보여주지 않음
					if (progressframe.isDisplayable()) {
						progressframe.setVisible(true);
						progressframe.setState(JFrame.NORMAL);
					}
				}
			}
		}
		else {
			systemtray.remove(trayicon);
			parent.parent.setVisible(true);
			parent.setVisible(true);
			if (progressframe.isDisplayable()) {
				progressframe.setVisible(true);
				progressframe.setState(JFrame.NORMAL);
			}
		}
		
		// 트레이 아이콘을 열 경우 자동 시작 플래그 해제
		parent.parent.autostarted = false;
	}
	
	/**
	 *  - 다운로드 진행창 구조 -
	 *  +--------------+
	 *  |   thumbnail  |
	 *  +--------------+
	 *  | progress bar |
	 *  | name         |
	 *  | 		   btn |
	 *  +--------------+
	 */
	public DownloadWorker(DownloadFrame parent, HashMap<String, Comics> selected_comics_map) {
		this.parent = parent;
		this.selected_comics_map = selected_comics_map;
		
		xmlhandler = new XMLHandler();
		
		progressframe = new JFrame("다운로드 중...");
		
		int downloadworker_w = 500;
		int downloadworker_h = 650;
		progressframe.setSize(downloadworker_w, downloadworker_h);
		
		// X 좌표 = 화면 가운데 좌표 => 화면의 오른쪽에 위치
		// Y 좌표 = 화면 세로에서 메인 프레임의 세로를 뺀 값의 절반
		int downloadworker_x = MainFrame.screenSize.width / 2;
		int downloadworker_y = (MainFrame.screenSize.height - downloadworker_h) / 2;
		progressframe.setLocation(downloadworker_x, downloadworker_y);
		
		progressframe.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		progressframe.setResizable(false);
		progressframe.setLayout(null);
		
		// Favicon 설정
		progressframe.setIconImage(new ImageIcon(PathInfo.getFaviconPath()).getImage());
		
		// 중지 액션 리스너 정의
		ActionListener actionlistenerStop = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				JOptionPane.showMessageDialog(progressframe, "중지되었습니다.", "알림", JOptionPane.INFORMATION_MESSAGE);
				mitemExit.setEnabled(true);
				cancel(true);
			}
		};

		// 트레이 아이콘의 팝업 메뉴, 시스템 트레이, 트레이 아이콘 정의
		systemtray = SystemTray.getSystemTray();
		traypopup = new PopupMenu();
		trayicon = new TrayIcon(new ImageIcon(PathInfo.getFaviconPath()).getImage(), "다우마루", traypopup);
		trayicon.setImageAutoSize(true);
		trayicon.addMouseListener(new MouseListener () {
			@Override
			public void mousePressed(MouseEvent e) {
			}
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() > 1) {
					ChangeTrayToFrame();
				}
			}
			@Override
			public void mouseEntered(MouseEvent e) {	
			}
			@Override
			public void mouseExited(MouseEvent e) {
			}
			@Override
			public void mouseReleased(MouseEvent e) {
			}
		});
		
		/* 
		 * 메뉴 아이템은 UTF-8 인코딩이 적용되지 않음 
		 * 컴파일을 MS949로 하면 한글이 출력되지만 이러면 이번에는 XML등 기타 파일이 MS949로 적용됨
		 */
		
		// 열기 아이템 정의
		MenuItem mitemOpen = new MenuItem("Open");
		mitemOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChangeTrayToFrame();
			}
		});
		
		// 중지 아이템 정의
		mitemCancle = new MenuItem("Stop");
		mitemCancle.addActionListener(actionlistenerStop);
		
		// 종료 아이템 정의
		// 종료의 경우 메인 프레임을 종료시키는 리스너를 부착
		// 다운로드 중일때는 종료할 수 없음 => 중지 시키고 종료해야함
		mitemExit = new MenuItem("Exit");
		mitemExit.addActionListener(this.parent.parent.actionlistenerExit);
		mitemExit.setEnabled(false);
		
		/** 
		 * 팝업 메뉴에 아이템 부착
		 * 팝업 메뉴 구조
		 * +-----+
		 * | 열기 |
		 * | 중지 |
		 * +-----+
		 * | 종료 |
		 * +-----+
		 */
		traypopup.add(mitemOpen);
		traypopup.add(mitemCancle);
		traypopup.addSeparator();
		traypopup.add(mitemExit);
		
		// 썸네일 패널 정의
		JPanel panelThumbnail = new JPanel();
		panelThumbnail.setSize(350, 490);
		panelThumbnail.setLocation(75, 20);
		panelThumbnail.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelThumbnail.setLayout(new BorderLayout());
		lblthumbnail = new JLabel();
		panelThumbnail.add(lblthumbnail, BorderLayout.CENTER);
		progressframe.add(panelThumbnail);
		
		// 상태 패널 정의
		JPanel panelStatus = new JPanel();
		panelStatus.setSize(300, 35);
		panelStatus.setLocation(100, 520);
		panelStatus.setLayout(new BorderLayout());
		UIManager.put("ProgressBar.background", Color.WHITE);
		UIManager.put("ProgressBar.foreground", Color.GREEN);
		progressbar = new JProgressBar();
		progressbar.setStringPainted(true);
		progressbar.setValue(0);
		lblname = new JLabel(" ");
		panelStatus.add(progressbar, BorderLayout.CENTER);
		panelStatus.add(lblname, BorderLayout.SOUTH);
		progressframe.add(panelStatus);
		
		this.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state")) {
					if (getState() == StateValue.STARTED) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								if (getState() == StateValue.STARTED) {
								}
							}
						});
					}
				}
				// progress 값이 변화하면 progress bar에 업데이트
				else if (evt.getPropertyName().equals("progress")) {
					progressbar.setValue((int)evt.getNewValue());
				}
			}
		});
		
		JPanel panelBtn = new JPanel();
		panelBtn.setSize(200, 35);
		panelBtn.setLocation(250, 560);
		panelBtn.setLayout(new FlowLayout(FlowLayout.RIGHT, 15, 5));
		progressframe.add(panelBtn);
		
		JButton btnHide = new JButton("숨기기");
		btnHide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ChangeFrameToTray(parent.parent.autostarted);			
			}
		});
		panelBtn.add(btnHide);
		
		JButton btnCancle = new JButton("중지");
		btnCancle.addActionListener(actionlistenerStop);
		panelBtn.add(btnCancle);
		
		progressframe.setVisible(true);
		
		// 자동 실행일 경우
		if (this.parent.parent.autostarted) {
			ChangeFrameToTray(this.parent.parent.autostarted);
		}
	}
	
	
	/**
	 * 선택한 만화 목록을 모두 다운로드
	 * 진행 경과를 DownloadDialog에 전송
	 */
	@Override
	protected Object doInBackground() throws Exception {
		String download_path = xmlhandler.getSavePath();
		Iterator<String> selected_comics_map_iter = selected_comics_map.keySet().iterator();
		int i = 0;
		while (selected_comics_map_iter.hasNext()) {
			int statusCode = Episode.StatusNormal;
			String selected_comics_name = selected_comics_map_iter.next();
			Comics comics = selected_comics_map.get(selected_comics_name);
			
			parent.lblstatus[i].setText(DownloadFrame.StateDownloading);
			
			// 만화에서 에피소드 목록 생성
			if (comics.CreateEpisodeList() == Episode.StatusError) {
				statusCode = Episode.StatusError;
			}
			
			// 만화 디렉토리 확인
			// 존재하지 않는 디렉토리이면 새로 생성
			// 사용할 수 없는 문자는 모두 @로 변환
			String comics_dir = download_path + XMLHandler.ReplaceInvalidChar(selected_comics_name) + File.separator;
			File outdir_file = new File(comics_dir);
			if (!outdir_file.exists()) {
				outdir_file.mkdirs();
			}
			
			ArrayList<Episode> episode_list = comics.getEpisodeList();
			int episode_list_size = episode_list.size();
			int gage = 0;
			setProgress(0);
			
			lblthumbnail.setIcon(GetThumbnail(selected_comics_name));
			
			// 만화 내의 에피소드를 순회하며 모두 다운로드
			for (Episode epi : episode_list) {
				if (this.isCancelled()) {
					return null;
				}
				lblname.setText(epi.GetName() + String.format(" (%d/%d)", gage + 1, episode_list_size));
				switch (epi.SaveEpisode(selected_comics_name)) {
					case Episode.StatusLock:
						statusCode = Episode.StatusLock;
						break;
					case Episode.StatusError:
						statusCode = Episode.StatusError;
						break;
					default:
				}
				setProgress((int)((gage + 1) * (100.0 / episode_list_size)));
				gage++;
			}
			setProgress(100);
			
			// 모두 다운로드 되면 다운로드 목록의 상태를 변경
			switch (statusCode) {
				case Episode.StatusLock:
					parent.lblstatus[i].setText(DownloadFrame.StateLock);
					parent.lblstatus[i].setForeground(Color.ORANGE);
					break;
				case Episode.StatusError:
					parent.lblstatus[i].setText(DownloadFrame.StateError);
					parent.lblstatus[i].setForeground(Color.RED);
					break;
				case Episode.StatusNormal:
					parent.lblstatus[i].setText(DownloadFrame.StateNormal);
					parent.lblstatus[i].setForeground(Color.GREEN);
					break;
				default:
			}
			
			Thread.sleep(1000);
			
			i++;
		}
		xmlhandler.close();
		return null;
	}
	
	/**
	 * 확인 메시지 출력
	 * 트레이 아이콘 메뉴 설정
	 * 부모의 버튼과 종료 옵션을 설정
	 */
	@Override
	protected void done() {
		if (!this.isCancelled()) {
			String donemsg = null;
			if (parent.parent.autostarted) {
				donemsg = "만화 업데이트 완료";
			}
			else {
				donemsg = "다운로드 완료";
			}
			trayicon.displayMessage(null, donemsg, TrayIcon.MessageType.INFO);
			JOptionPane.showMessageDialog(progressframe, donemsg, "알림", JOptionPane.INFORMATION_MESSAGE);
		}
		mitemCancle.setEnabled(false);
		mitemExit.setEnabled(true);
		parent.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		parent.btnOK.setVisible(false);
		parent.btnCancle.setText("종료");
		parent.btnCancle.setEnabled(true);
		progressframe.dispose();
	}
}
