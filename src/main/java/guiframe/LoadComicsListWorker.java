package guiframe;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import comicobj.Comics;
import comicobj.Episode;
import fileaccess.PathInfo;
import fileaccess.XMLHandler;

public class LoadComicsListWorker extends SwingWorker<Object, Integer>{
	private SelectFrame own;
	private JDialog dialog;
	private JLabel msg;
	private String page_list_url;
	
	private HashMap<String, Comics> selected_comics_map;
	
	// XML에서 모든 썸네일 경로를 로드
	XMLHandler xmlhandler = new XMLHandler();
	List<org.jdom2.Element> thumbnails_list = xmlhandler.LoadThumbnailPathList();
	
	private int retry_ctn = 3;
	
	public LoadComicsListWorker(SelectFrame own, String page_list_url, HashMap<String, Comics> selected_comics_map) {
		this.own = own;
		this.page_list_url = page_list_url;
		this.selected_comics_map = selected_comics_map;
		
		dialog = new JDialog(own, "알림");
		dialog.setSize(350, 100);
		dialog.setLocationRelativeTo(own);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setModal(false);
		dialog.setLayout(new BorderLayout());
		
		msg = new JLabel("만화 목록을 요청하는 중...");
		msg.setHorizontalAlignment(JLabel.CENTER);
		
		dialog.add(msg);
		
		// doInBackground()가 실행되면 dialog를 보여줌
		this.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("state")) {
					if (getState() == StateValue.STARTED) {
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								if (getState() == StateValue.STARTED) {
									dialog.setVisible(true);
								}
							}
						});
					}
				}
			}
		});
	}
	
	/**
	 * 썸네일을 다운받아 저장하고 XML에 목록을 저장하는 메소드
	 *  
	 *  - 메소드 처리 과정 - 
	 * 1. XML에 썸네일 경로가 존재하는지 확인
	 *  1-1. 존재한다면 로컬에 썸네일 파일이 존재한다고 간주하고 바로 경로를 반환
	 * 2. 존재하지 않는다면 웹에서 다운로드해 로컬에 저장
	 * 3. 다운로드가 완료되면 로컬에 저장한 경로를 XML에 저장
	 * 4. 저장 경로를 반환
	 */
	public String DownloadThumbnail(String thumbnail_url, String comics_name) {
		// 썸네일 경로 정의
		String thumbnail_path = PathInfo.getThumbnailsDirectory() + String.format("thumb_%s.jpg", XMLHandler.ReplaceInvalidChar(comics_name));
		
		// 존재한다면 바로 경로 반환
		for (org.jdom2.Element thumbnail_e : thumbnails_list) {
			if (thumbnail_e.getValue().equals(thumbnail_path)) {
				return thumbnail_path;
			}
		}
		
		while (true) {
			try {
				// 존재하지 않을 경우 웹에서 다운로드 받음
				URL url = new URL(thumbnail_url);
				URLConnection uCon = (HttpURLConnection) url.openConnection();
				// 브라우저 속성을 추가하지 않으면 403 Error가 발생
				uCon.setRequestProperty("User-Agent", Episode.user_agent);
				uCon.setConnectTimeout(5000);
				uCon.setReadTimeout(5000);
				uCon.setRequestProperty("Host", "marumaru.in");
				
				InputStream uin = uCon.getInputStream();
				OutputStream fout = new BufferedOutputStream(new FileOutputStream(thumbnail_path));
				
				int byteRead = 0;
				int buff_size = 4096;
				byte[] buff = new byte[buff_size];

				// 썸네일 다운로드
				while((byteRead = uin.read(buff)) != -1) {
					fout.write(buff, 0, byteRead);
				}
				
				// XML에 썸네일 경로 저장
				xmlhandler.SaveThumbnailPath(thumbnail_path);
				xmlhandler.SaveXMLFile();

				uin.close();
				fout.close();
			} catch (Exception e) {
				try {
					// 예외 발생시 10초 후 재시도
					// 3번 재시도하고 실패하면 종료
					if (retry_ctn > 0) {
						for (int i = 10; i > 0; i--) {
							msg.setText(String.format("응답 시간 초과(%d번 남음). %d초 후 재시도합니다.", retry_ctn, i));
							Thread.sleep(1000);
							msg.setText("썸네일을 생성하는 중...");
						}
						retry_ctn--;
					}
					else {
						msg.setText("재시도 회수 3번 초과. 3초 후 종료합니다.");
						Thread.sleep(3000);
						return null;
					}
				} catch (InterruptedException e1) {
					e1.printStackTrace();
					return null;
				}
			}
			return thumbnail_path;
		}
	}
	
	/**
	 * 만화 제목이 너무 긴 경우 <br> 태그를 통해 줄바꿈하는 메소드
	 * StringBuilder로 속도 향상
	 * @param original_name 만화 제목 원본
	 * @return "<html> 만화이름 조각1 <br> 만화이름 조각2 ... </html>"
	 */
	private String getDivComicsName(String original_name) {
		StringBuilder div_comics_sb = new StringBuilder();
		
		div_comics_sb.append("<html>");
		char arr_comics_name[] = original_name.toCharArray();
		
		for (int j = 0; j < original_name.length(); j++) {
			if (j > 0 && j % 15 == 0) {
				div_comics_sb.append("<br>");
			}
			div_comics_sb.append(arr_comics_name[j]);
		}
		
		div_comics_sb.append("</html>");
		return div_comics_sb.toString();
	}
	
	/**
	 * Background에서 만화 목록 페이지 하나를 불러와 Frame에 로드
	 * 처음 프레임이 만들어질 경우 또는 페이지를 이동할 경우 호출
	 * 
	 *    - 만화 패널 구조 - 
	 *   +----------------+
	 *   |                |
	 *   | h_thumbnail[i] | <- BorderLayout.NORTH
	 *   |                |
	 *   +----------------+ <---- h_panel[i]
	 *   |   h_chkbox[i]  | <- BorderLayout.CENTER
	 *   +----------------+
	 *   
	 */
	@Override
	protected Object doInBackground() throws Exception {
		while (true) {
			try {
				setAllDisable();
				Document comics_list_page = Jsoup.connect(page_list_url).timeout(10000).get();
				Elements comics_list = null;
				
				boolean searchMode = page_list_url.indexOf("keyword") > -1;
				
				// HTML parser를 통해 정보 추출
				
				// 키워드 검색일 경우
				if (searchMode) {
					// div.postbox = 검색 결과
					comics_list = comics_list_page.select("div.postbox").select("a");
				}
				// 페이지 번호 탐색일 경우
				else {
					// div.picbox = 만화 한칸
					// div.pagebox01 = 페이지 정보
					comics_list = comics_list_page.select("div.picbox");
					Elements pagebox = comics_list_page.select("div.pagebox01");

					// 현재 페이지 번호를 기억하고 라벨에 출력
					String curpage_str = pagebox.select("span.selected").text();
					own.curpage = Integer.parseInt(curpage_str);
					own.tfpage.setText(curpage_str);

					// 첫 페이지 출력시 마지막 페이지 URL을 파싱해 마지막 페이지 번호를 저장
					if (own.curpage == 1) {
						own.last_url = pagebox.first().select("a").last().attr("abs:href");
						own.lastpage = Integer.parseInt(own.last_url.substring(own.last_url.indexOf("p=") + 2, own.last_url.indexOf("&sort")));
					}
					
					// 이전 페이지, 다음 페이지의 URL 정의
					own.prev_url = String.format("http://marumaru.in/?c=1/40&p=%d&sort=gid", own.curpage - 1);
					own.next_url = String.format("http://marumaru.in/?c=1/40&p=%d&sort=gid", own.curpage + 1);
					// 10 페이지 전, 10페이지 후 URL 정의
					if (own.curpage - 10 > 1) {
						own.prev10_url = String.format("http://marumaru.in/?c=1/40&p=%d&sort=gid", own.curpage - 10);
					}
					else {
						own.prev10_url = own.first_url;
					}
					if (own.curpage + 10 < own.lastpage - 1) {
						own.next10_url = String.format("http://marumaru.in/?c=1/40&p=%d&sort=gid", own.curpage + 10);
					}
					else {
						own.next10_url = String.format("http://marumaru.in/?c=1/40&p=%d&sort=gid", own.lastpage);
					}
					
					own.tfSearch.setText("");
				}

				msg.setText("썸네일을 생성하는 중...");

				// div.pic = 썸네일이 있는 부분
				// div.sbjx = 만화 제목이 있는 부분
				int i = 0;
				for (Element comics_e : comics_list) {
					if (i >= 30) {
						break;
					}
					String comics_thumbnail;
					String comics_name;
					String comics_url;
					if (searchMode) {
						comics_thumbnail = comics_e.select("img[src]").first().attr("src");
						comics_name = XMLHandler.RemoveBrackets(comics_e.select("div.sbjbox").text());
						comics_url = comics_e.attr("abs:href");
					}
					else {
						comics_thumbnail = comics_e.select("div.pic img[src]").first().attr("src");
						comics_name = XMLHandler.RemoveBrackets(comics_e.select("div.sbjx a").first().text());
						comics_url = comics_e.select("div.sbjx a").first().attr("abs:href");
					}
					
					

					// 만화 제목이 너무 긴 경우 <br> 태그를 통해 줄바꿈
					// divided_comics_name = <html> ~ <br> ~ </html> 이 포함된 만화 이름
					String divided_comics_name = getDivComicsName(comics_name);

					// 썸네일을 다운로드받고 크기를 변경
					// 로컬에 썸네일이 있다면 로컬에서 로드, 없다면 웹에서 다운로드 받고 저장한 후 로드
					// panelSelect가 GridLayout이므로 이미지 사이즈를 정확하게 분할하지 않으면 
					// 남는 공간만큼 여백이 생기니 주의
					// 가령 width를 180으로 할 경우 오른쪽에 20만큼 빈 여백이 출력
					File thumbnail_file = new File(DownloadThumbnail(comics_thumbnail, comics_name));
					ImageIcon thumbnail_icon = XMLHandler.GetResizedImage(thumbnail_file, 200, 280);

					// 이미 선택한 만화들은 페이지 이동 후에도 남아있도록 만드는 과정
					// 여기서는 체크 박스가 변경되어도 선택한 만화 리스트에 추가하거나 제거하는 과정을 진행하면 안됨
					// 이벤트 리스너는 autochange flag가 false일때만 리스트에 추가하거나 제거함
					// 1. 이벤트 리스너의 자동 변경 여부를 true로 변경
					// 2. 선택한 만화 리스트에 있다면 체크 박스를 선택으로 만듦
					// 3. 선택한 만화 리스트에 없다면 체크 박스를 선택 해제로 만듦
					// 4. 이벤트 리스너의 자동 변경 여부를 false로 변경
					own.h_arrlistenerchkbox[i].setAutochange(true);
					if (selected_comics_map.containsKey(comics_name)) {
						own.h_chkbox[i].setEnabled(true);
						own.h_chkbox[i].setSelected(true);
					}
					else {
						own.h_chkbox[i].setEnabled(true);
						own.h_chkbox[i].setSelected(false);
					}
					own.h_arrlistenerchkbox[i].setAutochange(false);

					// 각 요소에 출력
					own.h_thumbnail[i].setIcon(thumbnail_icon);
					own.h_chkbox[i].setText(divided_comics_name);
					own.h_panel[i].setEnabled(true);
					own.h_panel[i].setVisible(true);

					// 원본 이름과 URL 저장
					own.h_originname[i] = comics_name;
					own.h_url[i] = comics_url;

					i++;
				}

				// 리스트가 30개보다 적을 경우 나머지는 보이지 않게 설정
				for (;i < 30; i++) {
					own.h_panel[i].setVisible(false);
					own.h_panel[i].setEnabled(false);
				}

				// 스크롤패인의 스크롤바를 가장 위쪽으로 변경
				own.scrollPaneSelect.getVerticalScrollBar().setValue(0);

				msg.setText("로드 완료");
				Thread.sleep(1000);

				// 1페이지일 경우 이전 페이지 버튼 비활성화
				// 마지막 페이지일 경우 다음 페이지 버튼 비활성화
				// 나머지는 모두 활성화
				if (own.curpage == 1) {
					own.btnMoveNext.setEnabled(true);
					own.btnMoveNext10.setEnabled(true);
					own.btnMoveLast.setEnabled(true);
				}
				else if (own.curpage == own.lastpage) {
					own.btnMoveFirst.setEnabled(true);
					own.btnMovePrev10.setEnabled(true);
					own.btnMovePrev.setEnabled(true);
				}
				else {
					own.btnMoveFirst.setEnabled(true);
					own.btnMovePrev10.setEnabled(true);
					own.btnMovePrev.setEnabled(true);
					own.btnMoveNext.setEnabled(true);
					own.btnMoveNext10.setEnabled(true);
					own.btnMoveLast.setEnabled(true);
				}
				
				own.tfpage.setEditable(true);
				
				own.btnSearch.setEnabled(true);
				own.tfSearch.setEditable(true);

				// 프레임 조작 버튼 활성화
				own.btnSave.setEnabled(true);
				own.btnCancle.setEnabled(true);

				return null;
			} catch (SocketTimeoutException socketexception) {
				// Timeout 발생시 10초 후 재시도
				// 3번 재시도하고 실패하면 종료
				if (retry_ctn > 0) {
					for (int i = 10; i >= 0; i--) {
						msg.setText(String.format("응답 시간 초과(%d번 남음). %d초 후 재시도합니다.", retry_ctn, i));
						Thread.sleep(1000);
						msg.setText("만화 목록을 요청하는 중...");
					}
					retry_ctn--;
				}
				else {
					msg.setText("재시도 회수 3번 초과. 3초 후 종료합니다.");
					Thread.sleep(3000);
					own.disposed = true;
					own.dispose();
					return null;
				}
			} catch (HttpStatusException httpstatsexception) {
				// 연결 실패시 5초 후 종료
				for (int i = 5; i >= 0; i--) {
					msg.setText(String.format("서버와 연결 실패. %d초 후 종료합니다.", i));
					Thread.sleep(1000);
				}
				own.disposed = true;
				own.dispose();
				return null;
			}
			catch (Exception e) {
				// 기타 예외 발생시 추적 후 종료
				e.printStackTrace();
				return null;
			}
		}
	}
	
	/**
	 *  SelectFrame의 모든 기능 비활성화
	 */
	public void setAllDisable() {
		// 체크박스 비활성화
		for (int i = 0; i < 30; i++) {
			own.h_chkbox[i].setEnabled(false);
		}
		
		// 페이지 이동 버튼 비활성화
		own.btnMoveFirst.setEnabled(false);
		own.btnMovePrev10.setEnabled(false);
		own.btnMoveNext.setEnabled(false);
		own.btnMovePrev.setEnabled(false);
		own.btnMoveNext10.setEnabled(false);
		own.btnMoveLast.setEnabled(false);
		
		own.tfpage.setEditable(false);
		
		own.btnSave.setEnabled(false);
		own.tfSearch.setEditable(false);
		
		// 프레임 조작 버튼 비활성화
		own.btnSave.setEnabled(false);
		own.btnCancle.setEnabled(false);
	}
	
	@Override
	protected void done() {
		dialog.dispose();
	}
}
